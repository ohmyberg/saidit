#!/usr/bin/env python3
import os
import shutil

docs = "/home/kevin/Documents/"
pics = "/home/kevin/Pictures/"
vids = "/home/kevin/Videos/"
music = "/home/kevin/Music/"

def moveto(dst):
    return lambda src: shutil.move(src, dst)

action = {
    'pdf': moveto(docs),
    'txt': moveto(docs),
    'jpg': moveto(pics),
    'png': moveto(pics),
    'svg': moveto(pics),
    'mp4': moveto(vids),
    'mov': moveto(vids),
    'mkv': moveto(vids),
    'flac': moveto(music),
    'mp3': moveto(music),
    'wma': moveto(music),
    'torrent': os.remove,
}

src_dir = '/home/kevin/Downloads'
for file in os.listdir(src_dir):
    ext = os.path.splitext(file)[1][1:]
    if ext in action:
        action[ext](os.path.join(src_dir, file))
